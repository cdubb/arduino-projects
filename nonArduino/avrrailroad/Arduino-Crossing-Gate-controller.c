/*
 * Arduino_Crossing_Gate_controller.c
 *
 * Created: 3/15/2012 4:25:31 PM
 *  Author: Marty
 *  Arduino Nano V3. ATMega328P 16mhz
 *
 * timer 0 = servo PWM OC0A, OC0B 1-2ms pulse every 24 ms
 * timer 1 = audio sample, realtime clock 8000hz
 * timer 2 = audio PWM clock -- running at 16mhz/255 = 62khz pwm period
 *
 * pb1 = pulled low to indicate occupancy
 * pb2 = occupancy/programming button
 * pb3 = OC2A audio PWM high bit
 * pb5 = LED on arduino
 *
 * pc0 = gate2 led output  
 * pc1 = flasher1-1 output
 * pc2 = flasher1-2 output
 * pc3 = gate1 led output
 * pc4 = flasher2-1 output
 * pc5 = flasher2-2 output
 *
 * pd0 = serial input
 * pd1 = serial output
 * pd3 = OC2B audio PWM low bit
 * pd6 = OC0A servo1 pulse output .5 to 2.5ms every 24ms
 * pd5 = OC0B servo2 pulse output .5 to 2.5ms every 24 ms
 * ADC6 analog input (50K ohm pot) during programming.
 * 
 *
 * when pb1 or pb2 are pulled low, the servo moves the flashers start.  2 seconds later
 * the servo moves to max at the defined servo speed.
 * when pb0 goes high, the servo moves to min at the defined servo speed.  Once the
 * servo is at min, the flashers stop.
 *
 * to enter program mode, turn the dial full counter-clockwise.  Flasher 1 will start flashing
 * in this mode, use the dial to set the servo min (gate up) position.  Note: do not attempt to drive the servo
 * beyond the physical limit.  If the servo is buzzing, it means the servo is trying to move and will
 * overheat if left in that position for more than a few moments.  When the desired servo position
 * is set, press and release the occupancy button. Flasher 2 will start flashing
 * in this mode, use the dial to set the servo max (gate down) position.  When finished, press and release
 * the occupancy button.  both flashers will flash simultaneously, and the servo will start moving
 * from min to max and back.  Use the dial to adjust the servo speed to the desired value.
 * when finished, press the occupancy button.  The flashers will alternate quickly 4 times 
 * indicating that the settings have been saved in EEPROM
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include "pcm_sample.h"

//#include <util/delay.h>	// only used for initial debugging

#define PULLUPS (1 << PB1) | (1<<PB2) 	// pullups on pb1 and pb2
#define FLASHER11 (1 << PC1)
#define FLASHER12 (1 << PC2)
#define FLASHER21 (1 << PC4)
#define FLASHER22 (1 << PC5)

#define SERVO1 (1<<PD6)
#define SERVO2 (1<<PD5)
#define GATE1LED (1 << PC3)
#define GATE2LED (1 <<PC0)
#define AUDIOHI (1 << PB3)
#define AUDIOLO (1 << PD3 )
#define PWMBIT (1 << PB1)
#define FLASHER1ON PORTC &= ~(FLASHER11 | FLASHER21)
#define FLASHER11ON PORTC &= ~(FLASHER11)
#define FLASHER21ON PORTC &= ~(FLASHER21)
#define FLASHER1OFF PORTC |= (FLASHER11 | FLASHER21)
#define FLASHER11OFF PORTC |= (FLASHER11)
#define FLASHER21OFF PORTC |= (FLASHER21)
#define FLASHER2ON PORTC &= ~(FLASHER12 | FLASHER22)
#define FLASHER12ON PORTC &= ~(FLASHER12)
#define FLASHER22ON PORTC &= ~(FLASHER22)
#define FLASHER2OFF PORTC |= (FLASHER12 | FLASHER22)
#define FLASHER12OFF PORTC |= (FLASHER12)
#define FLASHER22OFF PORTC |= (FLASHER22)
#define SERVOBITSON PORTD |= SERVO1 | SERVO2
#define SERVOBITSOFF PORTD &= ~(SERVO1 | SERVO2)
#define GATE1LEDOFF  PORTC |= GATE1LED
#define GATE1LEDON PORTC &= ~GATE1LED
#define GATE2LEDOFF  PORTC |= GATE2LED
#define GATE2LEDON PORTC &= ~GATE2LED
#define GATELEDOFF GATE1LEDOFF;GATE2LEDOFF
#define GATELEDON GATE1LEDON;GATE2LEDON
#define DIV1024CLOCK (1 << CS00) | (1 << CS02)
#define CLOCKSTOP  0
#define DIV64CLOCK (1 << CS00) | (1 << CS01)
#define SERVOMIN 1		//minimum value for servo
#define SERVOMAX 255	//maximum value for servo

#define ALLLEDOFF PORTC |= FLASHER11 | FLASHER12 | FLASHER21 | FLASHER22 | GATE1LED | GATE2LED
#define OCCUPIED (PINB & ((1 << PB1)|(1<<PB2))) != ((1 << PB1)|(1<<PB2))	// pb1 OR 2 = pulled low by occupancy detector
#define VACANT (PINB & ((1 << PB1)|(1<<PB2))) == ((1 << PB1)|(1<<PB2))
volatile unsigned int tickcount;
volatile unsigned char fasttick;	// 8 ticks = 1 millisec
volatile unsigned int millisec;	// increments once per millisec

volatile unsigned char servoticks;	// 192 ticks for each servo period
volatile unsigned char flashers;
volatile unsigned char seconds;
volatile unsigned char servomoving;	// 0 servo stopped, 1 servo moving
volatile uint16_t sample;		//pointer to current audio sample byte

unsigned char servo1min;
unsigned char servo1max;
unsigned char servo2min;
unsigned char servo2max;

unsigned char servodelay;

// realtime clock
// Timer1 compare A interrupt 16e6/2000 = interrupt 8000 times per sec (.000125sec)
ISR(TIMER1_COMPA_vect)
{
	// 8000 times a second update the audio pwm with the next sample
	// if the flashers are going or we are decaying after the last ding
	if(flashers==1 || sample != 0) {
		OCR2A = pgm_read_byte(&pcm_samples[sample++]);
		if(sample>2200){	//Normal bell repeats every .36 seconds
			if(flashers==1) {	// if the bell is ringing, repeat back to the beginning.
				sample=0;
			}			
			else if(sample > pcm_length) {	//at the last ding, let the bell decay as long as we have samples
				sample=0;
			}			
		}
	}
	else
	{
		if(OCR2A > 0) OCR2A--;	//fade to zero to eliminate the click at the end.
	}	

	if(servomoving) {
		// servo control sends pulses once every .024 seconds
		if(++servoticks >= 192) {
			// every 24 milliseconds start sending the servo pulse.
			// a servo pulse is between 1 and 2 milliseconds long.  We turn it on here
			// in 1 millisecond, we will start counter 0 to turn off the servo bits based on OCR0A and OCR0B
			// this gives us 255 possible servo positions
			PORTD |= SERVO1 | SERVO2;	
			//TIMSK0 |= (1<<TOIE0);	//enable the overflow interrupt so we can stop the timer as soon as it runs out.
			servoticks=0; 	// reset the servo pulse counter
		}
		// after the servo bits have been on for 1 millisecond, start timer 0 counting up.  When it
		// reaches the count specified in OCR0A and OCR0B we get an interrupt where we turn off the servo bit enough)
		if(servoticks == 8)	{
			// the servo bits have been on for 1 msec.  start timer0 to turn them off
			// start timer 0 running at 250khz
			// send an interrupt when the counter matches our servo postion
			TCNT0=0;
			TIFR0=(1<<OCF0A) | (1<<OCF0B);	// clear any pending interrupts (writing 1 to the bits clears them for whatever reaszon)
			TIMSK0 = (1 << OCIE0A) | (1 << OCIE0B);	// enable the output compare interrupts
		
		}
	}
		
	// 8 fast ticks is 1 millisec
	// The millisec counter gets cleared by the delay subroutine,  all we do here is increment it.
	if(fasttick++ >= 8) {
		millisec++;
		fasttick=0;
	}

	// 4000 ticks = half a second
	if(tickcount++ >= 4000) {
		//PORTB=(PORTB ^ FLASHER2) | PULLUPS;
		seconds++;		// actually counts half seconds
		/*if(seconds & 1) {
			 PORTB |= (1<<PB5);
		}
		else {
			 PORTB &= ~(1<<PB5);
		}*/
		
		tickcount=0;
		if(flashers == 1) {	// alternating flasher1, flasher2
			GATELEDON;
			if(seconds & 1) {
				FLASHER1ON;
				FLASHER2OFF;
			}
			else {
				FLASHER1OFF;
				FLASHER2ON;
			}	
		}
		else if(flashers == 2) {	// blink gate1 flasher1 (set gate one up position)
			ALLLEDOFF;
			FLASHER12OFF;FLASHER2OFF;
			if(seconds & 1) {
				FLASHER11ON;
			}
		}
		else if(flashers == 3) {	// blink gate 1 flasher2 (set gate one down position)
			ALLLEDOFF;
			if(seconds & 1) {
				FLASHER12ON;
			}
		}
		else if(flashers == 4) {	// blink gate 2 flasher 1 (gate two up position)
			ALLLEDOFF;
			if(seconds & 1) {
				FLASHER21ON;
			}
		}
		else if(flashers == 5)	{ // blink gate 2 flasher 2 (gate two down position)
			ALLLEDOFF;
			if(seconds & 1) {
				FLASHER22ON;
			}
		}
		else if(flashers == 6)	{ // blink both gates flasher1 and 2 (set speed)
			ALLLEDOFF;
			if(seconds & 1) {
				GATELEDON;
				FLASHER1ON;
				FLASHER2ON;
			}
		}
		else if(flashers == 7)	{ // blink gate led and flasher2
			ALLLEDOFF;
			if(seconds & 1) {
				GATELEDON;
				FLASHER2ON;
			}
		}
		else {
			FLASHER1OFF;
			FLASHER2OFF;
			GATELEDOFF;
		}
	}
}

// timer0 OVERFLOW -- not used 
//ISR(TIMER0_OVF_vect) {
	//TCCR0B= CLOCKSTOP;	// stop timer 0. we will restart it in 24msec using timer1
	//PORTB &= ~(1<<PB5);
//}

// timer 0 compare A -- turn off servo bit 1 at the end of the pulse
ISR(TIMER0_COMPA_vect) {
	PORTD &= ~SERVO1;			// make sure servo bit is off
	TIMSK0 &= ~(1<<OCIE0A);	// disable compare A interrupt
}
// timer 0 compare B -- turn off servo bit 2 at the end of the pulse
ISR(TIMER0_COMPB_vect) {
	PORTD &= ~SERVO2;	// make sure servo bit is off
	TIMSK0 &= ~(1 << OCIE0B);	// disable compare B interrupt
}

// delay -- stop processing for N milliseconds.
void delay(unsigned int ticks) {
	// set the millisecond tick timer to zero.  Interrupts will increment this for us
	// at .001 seconds per tick.
	millisec=0;
	while(millisec < ticks);
}

// toggle the arduino LED once per period mask secconds
// 1 = once per second		gates moving
// 2 = once per 2 seconds   occupied
// 4 = once every 4 seconds vacant
void flashled(unsigned char period ) {
	if(seconds & period) {
		PORTB |= (1<<PB5);
	}
	else {
		PORTB &= ~(1<<PB5);
	}
}
	
// moveto -- move the servos to the given target
void moveto(unsigned char target1, unsigned char target2) {
	while(OCR0A != target1 || OCR0B != target2) {
		flashled(1);		// while moving the gates flash the arduino LED once per second
		servomoving=1;		//turn on the servo
		// if we are programming the servo speed, then update the servo delay variable
		if(flashers==6) {
			servodelay=ADCH;
			if(servodelay < 1) servodelay=1;
			//if(servodelay > 50) servodelay=50;
		}
		delay(servodelay);
		//_delay_ms(20);
		if(OCR0A > target1) {
			OCR0A--;
		}
		if(OCR0A < target1) {
			OCR0A++;
		}
		if(OCR0B > target2) {
			OCR0B--;
		}
		if(OCR0B < target2) {
			OCR0B++;
		}
	}
	servomoving=0;	//turn off the servo
}

// doprogram -- change the program settings
// move the resistor full clockwise to start the programming.
//  The flashers will blink showing which value is being programmed
//   flasher1 = set servo1 minimum
//   flasher2 = set servo1 maximum
//   both flashers = set servo 1 min
//   gate led = set servo 2 max
//   gate led + flasher1 = set servo speed
// press the occupancy button to move to the next program value.  
// after all program values have been set, the unit returns to normal mode
// with the new values.
void doprogram(void) {
	//turn on the servos as long as we are programming
	servomoving=1;
	// get the servo1 min value (gate up)
	flashers=2;	// blink flasher1
	while(VACANT) {	//wait until they press the button
		servo1min=ADCH+1;
		OCR0A=servo1min;
	}
	eeprom_write_byte((uint8_t*) 0,servo1min);
	delay(20);		//debounce
	while(OCCUPIED) ;	//wait until the button is released
	delay(20);		//debounce
	
	// get the servo max value (gate down)
	flashers=3;	// blink flasher2
	while(VACANT) {
		servo1max=ADCH+1;
		OCR0A=servo1max;
	}
	eeprom_write_byte((uint8_t*) 1,servo1max);
	delay(20);		//debounce
	while(OCCUPIED) ;	//wait until the button is relased
	delay(20);		//debounce

	// get the servo2 min
	flashers=4;	// blink both flashers
	while(VACANT) {	//wait until they press the button
		servo2min=ADCH+1;
		OCR0B=servo2min;
	}
	eeprom_write_byte((uint8_t*) 2,servo2min);
	delay(20);		//debounce
	while(OCCUPIED) ;	//wait until the button is released
	delay(20);		//debounce
	
	// get servo2 max value (gate down)
	flashers=5;	// blink gate led
	while(VACANT) {
		servo2max=ADCH+1;
		OCR0B=servo2max;
	}
	eeprom_write_byte((uint8_t*) 3,servo2max);
	delay(20);		//debounce
	while(OCCUPIED) ;	//wait until the button is released
	delay(20);		//debounce
	
	//get the servo speed value
	flashers=6;		// flash both
	while(VACANT) {
		moveto(servo1max,servo2max);
		moveto(servo1min,servo2min);
	}
	flashers=0;
	eeprom_write_byte((uint8_t*) 4,servodelay);
	delay(20);		//debounce
	while(OCCUPIED) ;	//wait until the button is released
	servomoving=0;
	FLASHER1OFF;
	FLASHER2ON;
	GATELEDOFF;
	delay(100);
	FLASHER1ON;
	FLASHER2OFF;
	delay(100);
	FLASHER1OFF;
	FLASHER2ON;
	delay(100);
	FLASHER1ON;
	FLASHER2OFF;
	delay(100);
	FLASHER1OFF;
}	

// given a bit on port C, turn on that LED for half a second
void testit(unsigned char testled) {
	PORTC &= ~testled;
	delay(500);
	PORTC |= testled;

	/* blink that led until they push and release the occupancy button.
	while(VACANT) {
		if(seconds& 1) {
			PORTC &= ~testled;
		}
		else {
			PORTC |= testled;
		}
	}
	PORTC |= testled;	// turn of the led
	delay(10); //debounce
	// wait until they release the button
	while(OCCUPIED) {
	}
	delay(10);	//debounce
	*/
}

/* initialise everything */
void init(void)
{
	
	// set up the data direction registers
	DDRB = AUDIOHI | (1<<PB5);
	DDRC = FLASHER11 | FLASHER12 | FLASHER21 | FLASHER22 | GATE1LED | GATE2LED;
	DDRD = AUDIOLO | SERVO1 | SERVO2;

	PORTB=PULLUPS;
	
	// turn off all leds
	PORTC = FLASHER11 | FLASHER12 | FLASHER21 | FLASHER22 | GATE1LED | GATE2LED;
	flashers=0;
	
	
	//read eeprom variables for servo min/max and delay
	servo1min= eeprom_read_byte ((uint8_t*) 0);
	servo1max=eeprom_read_byte ((uint8_t*) 1);
	servo2min= eeprom_read_byte ((uint8_t*) 2);
	servo2max=eeprom_read_byte ((uint8_t*) 3);
	servodelay=eeprom_read_byte ((uint8_t*) 4);

	if(servo1min == 255 || servo1min == 0) {
		servo1min = 100;
	}
	if(servo2min == 255 || servo2min == 0) {
		servo2min = 100;
	}
	if(servo1max == 255 || servo1max == 0) {
		servo1max = 150;
	}
	if(servo2max == 255 || servo2max == 0) {
		servo2max = 150;
	}
		
	if(servodelay == 0) {
		servodelay = 1;
	}

	// set up timer 0 servo pulse timer
	// a servo pulse is 1 millisecond for 0 degrees and 2 milliseconds for 90 degrees.
	// the pulses are every 24 milliseconds.  The servo pulse  is started every 24 milliseconds
	// by the realtime clock (timer1).  Timer 0 compare match A and B are used to
	// set the length of the pulses
	TCCR0A=0;	// normal mode
	TCCR0B = DIV64CLOCK;	//250khz
	OCR0A = servo1min;	// start with the gates up
	OCR0B = servo2min;

	// set up the timer1 to provide the main realtime clock of 8000 ticks per second
	TCCR1A = 0;		// Normal operation
	TCCR1B = (1<<WGM12) | (1 << CS10);	// OCR1A CTC mode, divide by 1 clock (16mhz)
	OCR1A = 2000;	// upper limit of counter, generate 8000 overflow interrupts per second
	TIMSK1 = (1 << OCIE1A);	// interrupt on output compare A
	
	
	// set up the analog to digital converter
	ADMUX = (1<<ADLAR) 		//left adjust to get the high 8 bits (just read ADCH
		| (1 << REFS0)		// 5 volt reference
		| 6;			// ADC6 input  (actually mux1 | mux2)
		
	ADCSRA	= (1 << ADEN)	// Enable A/D conversion
		| (1 << ADSC)		// start the conversion
		| (1 << ADATE)		// enable auto conversion (free running)
		| (1<<ADPS2) | (1<<ADPS0);	// /32 Prescale


	// set up timer 2 for AUDIO pwm generation
    // clear OC0A on compare match 
    // set OC0A at BOTTOM, non-inverting mode
    // Fast PWM, 8bit
	TCCR2A = (1<<COM2A1) | (1<<WGM21) | (1<<WGM20);
    // Fast PWM, 8bit
    // Prescaler: clk/1 = 16MHz
    // PWM frequency = 16MHz / (255 + 1) = 62.5kHz
    TCCR2B =  (1<<CS20);
    // set initial duty cycle to zero */
    OCR2A = 0;

    sei(); //Enable interrupts
	delay(10);	//pause for a bit at startup to allow the ADC to rise above 0

}

int main(void)
{
	init();
 
	//on startup flash each led once
	
	servomoving=1; // force gates to the top while LEDs are flashing
	testit(FLASHER11);
	testit(FLASHER12);
	testit(GATE1LED);
	testit(FLASHER21);
	testit(FLASHER22);
	testit(GATE2LED);
	servomoving=0;	// turn off the servo now that it is at the top
	
	
	while(1) {
		flashled(4);	// toggle the led while we are waiting
		// if we have occupancy, make the gates go down
		if(OCCUPIED) {
			flashers=1;
			delay(2000);		//delay for 2 seconds after starting flashers
			moveto(servo1max,servo2max);
			while(OCCUPIED) flashled(2);
		}
		
		// if no occupancy, the gates go up.
		if(VACANT) {
			/*if(seconds & 1) {
				PORTB = PULLUPS | (1<<PB5);
			}
			else {
				PORTB = PULLUPS;
			}*/
			moveto(servo1min,servo2min);
			//turn off flashers
			flashers=0;
			delay(10);
			if(ADCH == 0) {			//if the ADC is full off, then program the settings
				doprogram();
			}
		}
	}	
}