/*
  Train gate controller
  
  Note:
    Servo:
        - To completely turn off servo it must be detached.
  
  
  Controls the operation of a train gate:
    * Raise/lower gate
    * Control motor speed and rotation
    * Flash 4 red LEDs
    * Play a bell sound
    * Use a photoresistor to detect movement, which trigger the gate operation
    
  Calculation (TODO):
    * Determine timing and speed of motor required to control the gate
    
    create 30 Jan 2013
    by Chris Wilder
*/
#include <IRremote.h>
#include <InsiginaKeyCodes.h>

//Gate operation constants
#define GATE_UP  0X00
#define GATE_DOWN  0X01
#define ALARM_OFF   0X02
#define ALARM_ON   0X03
#define LIGHTS_OFF   0X04
#define LIGHTS_ON   0X05

//Pin declaration
int irCtrlPin = 11;
int motorLPin = 8;
int motorRPin = 7;
int speedCtrlPin = 3;    //analogInput(petentiometer) --To--> PWM; H-bridge enable pin 
int potPin = A0;         //analog

//Initialize IR variables
IRrecv irrecv(irCtrlPin);
decode_results results;

boolean isMotorStopped = true;  //Flag to tell the motor. This is used when the gate is at it's max or min position.
//int pot = 0;  //Store value from petentiometer
int motorSpeed = 0;
int motorPeriod = 10;    //Time in milliseconds it will take the motor to raise/lower the gate

void setup() {
  Serial.begin(9600);    //setup Serial library at 9600 (for printing values back on the console)
  
  irrecv.enableIRIn(); // Start the IR receiver

  pinMode(potPin, INPUT);
  pinMode(motorLPin, OUTPUT);
  pinMode(motorRPin, OUTPUT);
  pinMode(speedCtrlPin, OUTPUT);
}

/*
  Get value from petentiometer and convert it to a speed.
*/
int getMotorSpeed() {
  int v;
  v = analogRead(potPin);
  Serial.println("pot input: " + v);
  v /=4;
  v = max(v, 90);  //???
  v = min(v, 255); //???
  return v;
}

/*
  Stop the motor
*/
void stopMotor() {
  motorSpeed = 0;
  analogWrite(speedCtrlPin, motorSpeed);
  isMotorStopped = true;
}

/*
   Raises/lowers the train gate
*/
void operateGate(int op) {
  
  //Check for valid operation (a bit of overkill but nice to robust)
  if(op == GATE_UP || op == GATE_DOWN) {
    
    //Spin motor backward (clockwise if facing the gate)
    if(op == GATE_UP) {
      digitalWrite(motorLPin, LOW);
      digitalWrite(motorRPin, HIGH);
  
    //Spin motor foreward (counter-clockwise if facing the gate)
    } else if(op == GATE_DOWN) {
      digitalWrite(motorLPin, HIGH);
      digitalWrite(motorRPin, LOW);
    }
  } else {
    Serial.print("Invalid gate operation");
    Serial.println(op);
  }
  
  
  motorSpeed = getMotorSpeed();
  analogWrite(speedCtrlPin, motorSpeed);
  isMotorStopped = false;
  Serial.print("motorSpeed:");
  Serial.println(motorSpeed);
  delay(motorPeriod);
  stopMotor();
}


void operateLights(int op) {}

void operateAlarm(int op) {}


void loop() {
    
  //If switch is activated, then run gateDown()
  if (irrecv.decode(&results)) {
    switch(results.value) {
      case INSIGNIA_TV_MODE_1:
        //Lower the gate
        operateGate(GATE_DOWN);
        
        //Enable the alarm
        operateAlarm(ALARM_ON);
        
        //Flash the LEDs
        operateLights(LIGHTS_ON);     
        
        break;
        
      //If swich is deactivate, delay 1 sec, then run gateUp()      
      case INSIGNIA_TV_MODE_2:
        delay(1000);    //delay to ensure the train is gone
        
        //Disable alarm
        operateAlarm(ALARM_OFF);
        
        //Raise the gate
        operateGate(GATE_UP);
        
        //Turn off LEDs
        operateLights(LIGHTS_OFF);
        
        break;
        
      default:
        print("Unsupported control: ");
        println(results.value);
    }
    
   
    irrecv.resume(); // Receive the next value        
  } 
  
}
