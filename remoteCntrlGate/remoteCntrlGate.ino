#include <IRremote.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.

#include <Servo.h> 
#include "InsigniaKeyCodes.h"

Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 

//#define INSIGNIA_TV_MODE_1 0x61A000FF
//#define INSIGNIA_TV_MODE_2 0x61A0807F

#define SAMPLE_RATE 8000

#define DOWN -1
#define UP 1

#define START_POS 180

int servoPin = 9; 
int pos = 0;    // variable to store the servo position 
int startPos = START_POS;
int curPos = startPos;
boolean downState = false;
boolean enableBell = false;

int irRecvPin = 11;
IRrecv irrecv(irRecvPin);
decode_results results;


void setup() 
{ 
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  myservo.attach(servoPin);  // attaches the servo on pin 9 to the servo object 
  myservo.write(startPos);
} 
 
/*
void rotationTest() {
  for(pos = startPos; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(30);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=startPos; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(30);                       // waits 15ms for the servo to reach the position 
  } 
}
*/

int moveServo(int dir, int initPos, int pos) {
  myservo.attach(servoPin);
  int curPos = initPos;
  int i;
  for(i = 0; i < pos; i++) {
    Serial.print("i = ");
    Serial.println(i);    
    Serial.print("curPos = ");
    Serial.println(curPos);
    //Serial.print("");
    //Serial.println();
    curPos += dir * 1;
    myservo.write(curPos);
    delay(30);
  }
  
  
  myservo.detach();
  return curPos;
}
 
void loop() { 
  /*
  if(!downState) {
    myservo.write(startPos);
    delay(30);
    curPos = moveServo(DOWN, startPos, 78);
    downState = true;
    moveServo(UP, curPos, 78);
  }
  */
  
    //If switch is activated, then run gateDown()
  if (irrecv.decode(&results)) {
    switch(results.value) {
      case INSIGNIA_TV_MODE_1:
        //Lower the gate
        if(!downState) {
          //myservo.write(START_POS);
          enableBell = true;
          delay(30);
          curPos = moveServo(DOWN, START_POS, 78);
          Serial.println(curPos);
          downState = true;
          enableBell = false;
        }
        
        break;
        
      //If swich is deactivate, delay 1 sec, then run gateUp()      
      case INSIGNIA_TV_MODE_2:
         if(downState) {
          enableBell = false;
          moveServo(UP, curPos, 78);
          downState = false;
        }
        
        break;
        
      default:
        Serial.print("Unsupported control: ");
        Serial.println(results.value);
    }
    irrecv.resume(); // Receive the next value        
  } 
  
}
