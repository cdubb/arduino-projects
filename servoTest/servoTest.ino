// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 


#define DOWN -1
#define UP 1

int servoPin = 9; 
int pos = 0;    // variable to store the servo position 
int startPos = 180;
boolean stopFlg = false;

void setup() 
{ 
  Serial.begin(9600);
  myservo.attach(servoPin);  // attaches the servo on pin 9 to the servo object 
} 
 
/*
void rotationTest() {
  for(pos = startPos; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(30);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=startPos; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(30);                       // waits 15ms for the servo to reach the position 
  } 
}
*/

int moveServo(int dir, int initPos, int pos) {
  int curPos = initPos;
  int i;
  for(i = 0; i < pos; i++) {
    Serial.print("i = ");
    Serial.println(i);    
    Serial.print("curPos = ");
    Serial.println(curPos);
    //Serial.print("");
    //Serial.println();
    curPos += dir * 1;
    myservo.write(curPos);
    delay(30);
  }
  
  return curPos;
}
 
void loop() { 
  if(!stopFlg) {
    myservo.write(startPos);
    delay(30);
    int curPos = moveServo(DOWN, startPos, 78);
    stopFlg = true;
    moveServo(UP, curPos, 78);
  }
  
}
