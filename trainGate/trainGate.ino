/*
  
  Automatic train gate.

  NOTE: This program uses the internal timer directly. As a result, the timer0 interrupts fail to trip b/c the 
  USB and serial connection use timer0. To get the gat to move, a Serial.println() was added to the moveTo(...)
  procedure. 

  TODO: Add pullup a switch to operate the gate.
  TODO: try and provide a direct power source and eliminate the USB to see if that fixes the
  timer0 ISR issue.

  1. The gate goes down:
      the lights flash
      the bell rings

 2. The gate goes up:
     the lights stop flashing
     the bell stops ringing
  
*/

//#include <Servo.h> 
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "bellSample.h"


#define SAMPLE_RATE 8000
#define DIV64CLOCK (1 << CS00) | (1 << CS01)    //B00000011 = clk/64 (From prescaler)
#define PB3 B00001000     //PORTB
#define PC0 B00000001     //PORTC
#define PC3 B00001000     //PORTC
#define PD3 B00001000     //PORTD
#define PD6 B01000000     //PORTD
#define AUDIOHI PB3 //Enable write on pin ~11
//#define AUDIOLO PD3 //Enable write on pin ~3    (TODO: Is this needed?)
#define SERVO PD6
#define LED1 PC0    //Enable write on pin A0
#define LED2 PC3    //Enable write on pin A1
#define DOWN -1
#define UP 1
#define ENABLED 1
#define DISABLED 0
#define START_POS 70


#define FLASHER1OFF PORTC &= ~LED1
#define FLASHER1ON PORTC |= LED1
#define FLASHER2OFF PORTC &= ~LED2
#define FLASHER2ON PORTC |= LED2


volatile unsigned int tickCount;  // increments once per iteration in the main (real-time) loop.
volatile unsigned char fastTick;  // 8 ticks = 1 millisec
volatile unsigned int millisec;   // increments once per millisec

int gateOpenServoPos = START_POS;
int gateClosedServoPos = 255;//gateOpenServoPos + 255;
int servoDelay = 30;    //Controls the servo speed
char servoState = DISABLED;
volatile int servoTicks;  // 192 ticks for each servo period
char flasherSwitchState = 0x0;   // Switch between flashers


boolean downState = false;
boolean enableBell = false;
boolean enableFlashers = false;

//int irRecvPin = 5;//11;
//IRrecv irrecv(irRecvPin);
//decode_results results;

volatile uint16_t sample;   //pointer to current audio sample byte

// realtime clock
// Timer1 compare A interrupt 16e6/2000 = interrupt 8000 times per sec (.000125sec)
ISR(TIMER1_COMPA_vect)
{
  // 8000 times a second update the audio pwm with the next sample
  // if the flashers are going or we are decaying after the last ding
  if(enableBell==true || sample != 0) {
    OCR2A = pgm_read_byte(&pcm_samples[sample++]);
    
    if(sample>2200){  //Normal bell repeats every .36 seconds
      if(enableBell==true) { // if the bell is ringing, repeat back to the beginning.
        sample=0;
      }     
      else if(sample > pcm_length) {  //at the last ding, let the bell decay as long as we have samples
        sample=0;
      }     
    }
  }
  else
  {
    if(OCR2A > 0) OCR2A--;  //fade to zero to eliminate the click at the end.
  }

  if(servoState == ENABLED) {
    // servo control sends pulses once every .024 seconds
    if(++servoTicks >= 192) {
      // every 24 ms start sending the servo pulse.
      // a servo pulse is between 1 and 2 ms long. We turn it on here
      // in 1 ms, we will start counter 0 to turn off the servo bits based on OCR0A
      // this gives us 255 possible servo positions
      PORTD |= SERVO; 
      //TIMSK0 |= (1<<TOIE0); //enable the overflow interrupt so we can stop the timer as soon as it runs out.
      servoTicks=0;   // reset the servo pulse counter
    }

    // after the servo bits have been on for 1 millisecond, start timer 0 counting up.  When it
    // reaches the count specified in OCR0A and OCR0A we get an interrupt where we turn off the servo bit enough)
    if(servoTicks == 8) {
      // the servo bits have been on for 1 msec.  start timer0 to turn them off
      // start timer 0 running at 250khz
      // send an interrupt when the counter matches our servo postion
      TCNT0=0;
      TIFR0=(1<<OCF0A);  // clear any pending interrupts (writing 1 to the bits clears them for whatever reaszon)
      TIMSK0 = (1 << OCIE0A); // enable the output compare interrupts
    
    }
  }

  // 8 fast ticks = 1 ms
  // the millisec count is reset in the __delay subroutine. millisec is just incremented here.
  if(fastTick++ >= 8) {
    millisec++;
    fastTick = 0;
  }

  // 4000 ticks = 1/2 sec
  if(++tickCount >= 4000)  {
    tickCount = 0;

    if(enableFlashers == true) {
      
      if(flasherSwitchState == 0) {
        FLASHER1ON;
        FLASHER2OFF;
        flasherSwitchState = 1;
      } else {
        FLASHER1OFF;
        FLASHER2ON;
        flasherSwitchState = 0;
      }

//      flasherSwitchState = ~flasherSwitchState + 1;
    } else {
      FLASHER1OFF;
      FLASHER2OFF;
      flasherSwitchState = 0x0;
   }

  }

} 

// timer 0 compare A -- turn off servo bit at the end of the pulse (equivalent to servo detach)
ISR(TIMER0_COMPA_vect)
{ 
  //PORTD = B00000000;  //DEBUGGING ENABLE LED
  //PORTC |= LED1;
//PORTC |= LED2;

  
  PORTD &= ~SERVO;            // make sure the servo bit is off
  TIMSK0 &= ~(1 << OCIE0A);     // disable compare A interrupt
}

//void setup() 
void init(void)
{   
  Serial.begin(9600);

    // set up the data direction registers
  DDRB = AUDIOHI;  
  DDRC = LED1 | LED2;
  DDRD = SERVO;


  // setup time 0 servo pulse timer
  // a servo pulse is 1 ms for 0 degrees and 2 ms for 90 degrees.
  // the pulses are every 24 milliseconds. The servo pulse is started ever 24 milliseconds (NOTE: My original implementation use 30 ms)
  // by the realtime clock (timer1). Time 0 compare match A and B are used to set the length of the pulses
  TCCR0A = 0;   // normal mode
  TCCR0B = DIV64CLOCK;  //250khz
  OCR0A = gateOpenServoPos; //start with the gates up?

  // set up the timer1 to provide the main realtime clock of 8000 ticks per second
  TCCR1A = 0;   // Normal operation
  TCCR1B = (1 << WGM12) | (1 << CS10);  // OCR1A CTC mode, divide by 1 clock (16mhz)
  OCR1A = 2000; // upper limit of counter, generate 8000 overflow interrupts per second
  TIMSK1 = (1 << OCIE1A); // interrupt on output compare A


  // set up timer 2 for AUDIO pwm generation
    // clear OC0A on compare match 
    // set OC0A at BOTTOM, non-inverting mode
    // Fast PWM, 8bit
  TCCR2A = (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);
  // Fast PWM, 8bit
  // Prescaler: clk/1 = 16MHz
  // PWM frequency = 16MHz / (255 + 1) = 62.5kHz
  TCCR2B =  (1 << CS20);
  // set initial duty cycle to zero */
  OCR2A = 0;

  sei(); //Enable interrupts
  __delay(20);  //pause for a bit at startup to allow the ADC to rise above 0
} 

// delay -- stop processing for N milliseconds.
void __delay(unsigned int ticks) {
  // set the millisecond tick timer to zero.  Interrupts will increment this for us
  // at .001 seconds per tick.
  millisec=0;
  while(millisec < ticks);
}

// moveto -- move the servos to the given target
void moveto(unsigned char target) {
  //Serial.println("calling moveto");
  
  while(OCR0A != target) {
    
    servoState = ENABLED;    //turn on the servo
    Serial.println(OCR0A);
    __delay(10);
    
    if(OCR0A > target) {
      OCR0A--;
    }
    if(OCR0A < target) {
      OCR0A++;
    }
  }
  servoState = DISABLED;  //turn off the servo
  //Serial.print("2 servoState: ");
  //Serial.println(servoState);
}
 
int main(void) 
{ //TODO: Fix logic

  init();

  __delay(500);

  while(1) {
    //Move down
    //if(!downState) {
        //myservo.write(START_POS);
        enableFlashers = true;
        enableBell = true;
        __delay(10);
        moveto(gateOpenServoPos);
        
        __delay(3000);
        /*curPos = moveServo(DOWN, START_POS, 78);
        Serial.println(curPos);*/
  
        downState = true;
        //enableBell = false;
  
    //} else if(downState) {
      enableBell = false;
      enableFlashers = false;
      moveto(gateClosedServoPos);
      downState = false;
    //}
  
  
    __delay(5000);
  }
  
}

