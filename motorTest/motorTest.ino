/*
  Train gate controller
  
  Controls the operation of a train gate:
    * Raise/lower gate
    * Control motor speed and rotation
    * Flash 4 red LEDs
    * Play a bell sound
    * Use a photoresistor to detect movement, which trigger the gate operation
    
  Calculation (TODO):
    * Determine timing and speed of motor required to control the gate
    
    create 30 Jan 2013
    by Chris Wilder
*/
#include <IRremote.h>
#include <InsiginaKeyCodes.h>

//Gate operation constants
#define GATE_UP  0X00
#define GATE_DOWN  0X01
#define ALARM_OFF   0X02
#define ALARM_ON   0X03
#define LIGHTS_OFF   0X04
#define LIGHTS_ON   0X05

//Pin declaration
int irCtrlPin = 11;
int motorLPin = 8;
int motorRPin = 7;
int speedCtrlPin = 9;    //analogInput(petentiometer) --To--> PWM; H-bridge enable pin 
int potPin = A0;         //analog

//Initialize IR variables
IRrecv irrecv(irCtrlPin);
decode_results results;

boolean isMotorStopped = true;  //Flag to tell the motor. This is used when the gate is at it's max or min position.
//int pot = 0;  //Store value from petentiometer
int motorSpeed = 0;
int motorPeriod = 10;    //Time in milliseconds it will take the motor to raise/lower the gate

void setup() {
  Serial.begin(9600);    //setup Serial library at 9600 (for printing values back on the console)
  
  irrecv.enableIRIn(); // Start the IR receiver
  
  //pinMode(irCtrlPin, INPUT);
  pinMode(potPin, INPUT);
  pinMode(motorLPin, OUTPUT);
  pinMode(motorRPin, OUTPUT);
  pinMode(speedCtrlPin, OUTPUT);
  
  //while (! Serial);
  //Serial.println("Speed 0 to 255");
  
 digitalWrite(motorLPin, LOW);
 digitalWrite(motorRPin, HIGH);
}

/*
  Get value from petentiometer and convert it to a speed.
*/
int getMotorSpeed() {
  int v;
  v = analogRead(potPin);
  Serial.println("pot input: ");
  Serial.println(v);
  v /=4;
  v = max(v, 90);  //???
  v = min(v, 255); //???
  return v;
}

/*
  Stop the motor
*/
void stopMotor() {
  motorSpeed = 0;
  analogWrite(speedCtrlPin, motorSpeed);
  isMotorStopped = true;
}

/*
   Raises/lowers the train gate
*/
void operateGate(int op) {
  
  //Check for valid operation (a bit of overkill but nice to robust)
  if(op == GATE_UP || op == GATE_DOWN) {
    
    //Spin motor backward (clockwise if facing the gate)
    if(op == GATE_UP) {
      digitalWrite(motorLPin, LOW);
      digitalWrite(motorRPin, HIGH);
  
    //Spin motor foreward (counter-clockwise if facing the gate)
    } else if(op == GATE_DOWN) {
      digitalWrite(motorLPin, HIGH);
      digitalWrite(motorRPin, LOW);
    }
  } else {
    Serial.print("Invalid gate operation");
    Serial.println(op);
  }
  
  
  //motorSpeed = getMotorSpeed();
  motorSpeed = 254;
  analogWrite(speedCtrlPin, motorSpeed);
  isMotorStopped = false;
  Serial.print("motorSpeed:");
  Serial.println(motorSpeed);
  //delay(motorPeriod);
  
  //stopMotor();
}

void speedTestWave() {
  
 digitalWrite(motorLPin, LOW);
 digitalWrite(motorRPin, HIGH);
 for(int motorSpeed = 0 ; motorSpeed <= 255; motorSpeed +=5) {
   analogWrite(speedCtrlPin, motorSpeed);
   Serial.print("Speed1: ");
   Serial.println(motorSpeed);   
   delay(300);      
 }
 for(int motorSpeed = 255 ; motorSpeed >= 0; motorSpeed -=5) {
   analogWrite(speedCtrlPin, motorSpeed); 
   Serial.print("Speed2: ");
   Serial.println(motorSpeed);  
   delay(300);      
 }
}

void speedTestTicks() {
  for(int ticks = 0; ticks < 255; ticks+=10) {
    analogWrite(speedCtrlPin, 255-ticks);
    //analogWrite(speedCtrlPin, 255);    
    delay(100);
    //analogWrite(speedCtrlPin, 64);
    //delay(150);
  }
}


/*
  The following must appear in the setup section.
  
  //while (! Serial);
  //Serial.println("Speed 0 to 255");
*/
void speedTestSerialInput() {
   digitalWrite(motorLPin, LOW);
   digitalWrite(motorRPin, HIGH);
  
   if (Serial.available()){
    
    int motorSpeed = Serial.parseInt();

    
    if (motorSpeed >= 0 && motorSpeed <= 255) { 
       Serial.print("Speed: ");
    Serial.println(motorSpeed);   
      analogWrite(speedCtrlPin, motorSpeed);  
    }
    
  }
}

void loop() {
  //operateGate(GATE_DOWN);
//  speedTestWave();
  speedTestTicks();
  
  //speedTestSerialInput();
  
  
    /*
  //If switch is activated, then run gateDown()
  if (irrecv.decode(&results)) {
    switch(results.value) {
      case INSIGNIA_TV_MODE_1:
        //Lower the gate
        operateGate(GATE_DOWN);
        
        //Enable the alarm
        operateAlarm(ALARM_ON);
        
        //Flash the LEDs
        operateLights(LIGHTS_ON);     
        
        break;
        
      //If swich is deactivate, delay 1 sec, then run gateUp()      
      case INSIGNIA_TV_MODE_2:
        delay(1000);    //delay to ensure the train is gone
        
        //Disable alarm
        operateAlarm(ALARM_OFF);
        
        //Raise the gate
        operateGate(GATE_UP);
        
        //Turn off LEDs
        operateLights(LIGHTS_OFF);
        
        break;
        
      default:
        print("Unsupported control: ");
        println(results.value);
    }
    
   
    irrecv.resume(); // Receive the next value       
    
  } */
  
}
