/*
 * IRremote: IRrecvDemo - demonstrates receiving IR codes with IRrecv
 * An IR detector/demodulator must be connected to the input RECV_PIN.
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 *
 * Example from: http://www.righto.com/2009/08/multi-protocol-infrared-remote-library.html
 *
 * Modified by Chris Wilder
 */

#include <IRremote.h>
//InsiginaKeyCodes.h
int RECV_PIN = 11;
int ledPin = 12;

IRrecv irrecv(RECV_PIN);

decode_results results;

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  pinMode(ledPin, OUTPUT);  //LED indicator
}

void loop() {
   //digitalWrite(ledPin, HIGH);
   //Serial.println("Booger");
  if (irrecv.decode(&results)) {
    Serial.print("CODE: ");
    Serial.println(results.value, HEX);
    
    digitalWrite(ledPin, HIGH);  //Turn on the LED
    delay(1000);
    irrecv.resume(); // Receive the next value        
  } else {
    digitalWrite(ledPin, LOW);  //Turn off the LED
  }
  
  
}
