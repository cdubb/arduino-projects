/**
 *
 * TODO: Use the tone library for the bell sound.
 */
#include "Arduino.h"
#include "pins_arduino.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include "bellSample.h"

#define SAMPLE_RATE 8000

#define PB3 B00001000
#define PD3 B00001000

#define AUDIOHI PB3	//Enable write on pin ~11
#define AUDIOLO PD3	//Enable write on pin ~3
#define PWMBIT (1 << PB1)	//TODO: ???

boolean enableBell = false;

volatile uint16_t sample;   //pointer to current audio sample byte

// realtime clock
// Timer1 compare A interrupt 16e6/2000 = interrupt 8000 times per sec (.000125sec)
ISR(TIMER1_COMPA_vect)
{
  // 8000 times a second update the audio pwm with the next sample
  // if the flashers are going or we are decaying after the last ding
  if(enableBell==true || sample != 0) {
    OCR2A = pgm_read_byte(&pcm_samples[sample++]);
    //analogWrite(11, OCR2A);
    
    if(sample>2200){  //Normal bell repeats every .36 seconds
      if(enableBell==true) { // if the bell is ringing, repeat back to the beginning.
        sample=0;
      }     
      else if(sample > pcm_length) {  //at the last ding, let the bell decay as long as we have samples
        sample=0;
      }     
    }
  }
  else
  {
    if(OCR2A > 0) OCR2A--;  //fade to zero to eliminate the click at the end.
  }
} 

void setup() 
{ 
  Serial.begin(9600);
  /* Initialize the PWM  for audio (http://avrpcm.blogspot.com/2010/11/playing-8-bit-pcm-using-any-avr.html)*/
  /* use OC1A pin as output */
 // DDRD = (1 << PD5);
 //pinMode(11, OUTPUT);
 
 //noInterrupts();           // disable all interrupts
	// set up the data direction registers
	DDRB = AUDIOHI;
	DDRD = AUDIOLO;


	// set up the timer1 to provide the main realtime clock of 8000 ticks per second
	TCCR1A = 0;		// Normal operation
	TCCR1B = (1 << WGM12) | (1 << CS10);	// OCR1A CTC mode, divide by 1 clock (16mhz)
	OCR1A = 2000;	// upper limit of counter, generate 8000 overflow interrupts per second
	TIMSK1 = (1 << OCIE1A);	// interrupt on output compare A
	


	// set up timer 2 for AUDIO pwm generation
    // clear OC0A on compare match 
    // set OC0A at BOTTOM, non-inverting mode
    // Fast PWM, 8bit
	TCCR2A = (1 << COM2A1) | (1 << WGM21) | (1 << WGM20);
    // Fast PWM, 8bit
    // Prescaler: clk/1 = 16MHz
    // PWM frequency = 16MHz / (255 + 1) = 62.5kHz
    TCCR2B =  (1 << CS20);
    // set initial duty cycle to zero */
    OCR2A = 0;

//    sei(); //Enable interrupts
    //interrupts();
//	delay(10);	//pause for a bit at startup to allow the ADC to rise above 0
  enableBell = true;
} 
 
 
 
void loop() { 
    //enableBell = true;
    //delay(500);
    //enableBell = false;
    //delay(500);
    //while(1); //Stop forever
   
}
